import heapq
import sys

def sol2():
    N, k = map(int, sys.stdin.readline().split())

    cd = []
    cw = []
    for i in range(N):
        d, w = map(int, sys.stdin.readline().split())
        cd.append(d)
        cw.append(w)

    counters = []
    for i in range(k):
        if not len(cw):
            break
        heapq.heappush(counters, (cw.pop(0), -i, cd.pop(0)))

    out = []

    while len(cw):
        m = heapq.heappop(counters)
        t = [heapq.heappop(counters) for i in range(k) if m[0] == counters[0][0]]
        t.insert(0, m)
        
        wi = m[0]
        counters = [(w-wi, ki, d) for w, ki, d in counters]

        out += t

        for wi, ki, d in reversed(t):
            if not len(cw):
                break
            heapq.heappush(counters, (cw.pop(0), ki, cd.pop(0)))
            
    for i in range(len(counters)):
        out.append(heapq.heappop(counters))

    result = 0
    for i, d in enumerate([d for wi, ki, d in out]):
        result += (i+1)*d

    print(result)
sol2()