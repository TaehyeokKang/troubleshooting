import sys

with open('output/output.txt', 'rt') as f:
    out = f.readline()

with open('output/runtime.txt', 'rt') as f:
    runtime = f.readline()

with open('output/ans_output.txt', 'rt') as f:
    ans_out = f.readline()

with open('output/ans_runtime.txt', 'rt') as f:
    ans_runtime = f.readline()

with open('output/input.txt', 'rt') as f:
    N, k = f.readline().split()

print('* TEST INFO')
print('N={}, k={}'.format(N, k))
print('[Your Code]')
print('OUTPUT: ', out)
print('RUNTIME: ', int(runtime)/1000)
print('\n[Ans Code]')
print('OUTPUT: ', ans_out)
print('RUNTIME: ', int(ans_runtime)/1000)

if (out == ans_out) and (int(runtime) <= int(ans_runtime)):
    print("Solve the problem perfectly.")
    sys.exit(0)
if (out == ans_out) and (int(runtime) <= 1000):
    print("Solve the problem.")
    sys.exit(0)
elif (out == ans_out):
    sys.exit('Algorithm is valid, but it has timed out.')
else:
    sys.exit('Algorithm is not valid.')