import sys

def sol():
    N, k = map(int, sys.stdin.readline().split())

    clients = []
    for i in range(N):
        d, w = map(int, sys.stdin.readline().split())
        clients.append([d, w])

    counters = []
    out = []
    oi = None

    while True:
        if (len(clients) != 0):
            if oi == None:
                for i in range(k):
                    counters.append(clients.pop(0))
            else:
                for i in oi:
                    if len(clients) != 0:
                        counters.insert(i, clients.pop(0))
                    else:
                        break

        m = min(counters, key = lambda t: t[1])
        mins = [i for i in counters if m[1] == i[1]]

        oi = list(map(counters.index, mins))

        for i in reversed(oi):
            out.append(counters.pop(i)[0])

        for i in range(len(counters)):
            counters[i][1] -= m[1]

        if len(counters) == 0:
            break

    result = 0

    for i, d in enumerate(out):
        result += (i+1)*d

    print(result)

sol()