import sys
import heapq

def sol3():
    scanf = sys.stdin.readline
    N, k = map(int, scanf().split())
    cd = []
    cw = []
    cd_a = cd.append
    cw_a = cw.append
    
    for i in range(N):
        d, w = map(int, scanf().split())
        cd_a(d)
        cw_a(w)

    counters = [(0, i) for i in range(k)]
    heapq.heapify(counters)

    out = []
    qpop = heapq.heappop
    qpush = heapq.heappush
    o_a = out.append
    for i in range(N):
        t, ki = qpop(counters)
        t += cw[i]
        qpush(counters, (t, ki))
        o_a((t, -ki, cd[i]))

    result = 0
    for i, (_, _, d) in enumerate(sorted(out, key=lambda s: (s[0], s[1]))):
        result += (i+1)*d
    print(result)
sol3()