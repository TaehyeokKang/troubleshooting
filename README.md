# Troubleshooting

## 문제 및 풀이 게시 규칙

* 문제 게시: `{문제 게시자 username}-{YYMMDD}/README.md`  
* 풀이 게시: `{문제 게시자 username}-{YYMMDD}/solution/{풀이 게시자 username}/{filename}.{extension}`

## AutoMark
### Merge Requset

* target branch: `master`
* title: `Submit {문제 게시자 username}-{YYMMDD}/solution/{풀이 게시자 username}/{filename}.{extension}`

### Push

* Commit Message:
```
Commit Message Title

Submit {문제 게시자 username}-{YYMMDD}/solution/{풀이 게시자 username}/{filename}.{extension}
```