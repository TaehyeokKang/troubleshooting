nvar=524288

if [ "$1" = "python" ]; then
    ulimit -m $nvar
    StartTime=$(date +%s%N)
	python $SUBMIT < $INPUT_FILE > output/$2
    EndTime=$(date +%s%N)
elif [ "$1" = "java" ]; then
    ulimit -m $nvar
    StartTime=$(date +%s%N)
	java $FILE_PATH < $INPUT_FILE > output/$2
    EndTime=$(date +%s%N)
elif [ "$1" = "cpp" ]; then
    ulimit -m $nvar
	StartTime=$(date +%s%N)
	./main < $INPUT_FILE > output/$2
    EndTime=$(date +%s%N)
fi
echo $(($(($EndTime - $StartTime))/1000000)) > output/$3