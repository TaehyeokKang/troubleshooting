# n, k
# id, w
# 
# 1 < n, k < 100,000
# 1 < id < 1,000,000
# 1 < w < 20

import random
import sys

def main(N:int=None, k:int=None):
    builder = []
    if k == None:
        k = random.randint(1, 99999)
    if N == None:
        N = random.randint(k+1, 100000)

    builder.append('{} {}'.format(N, k))

    ids = random.sample(range(1, 1000001), N)
    
    for i in range(N):
        w = random.randint(1, 20)
        builder.append('{} {}'.format(ids[i], w))

    string = '\n'.join(builder)

    with open('input.txt', 'wt') as f:
        f.write(string)

N = None
k = None
if '-N' in sys.argv:
    i = sys.argv.index('-N')
    N = int(sys.argv[i+1])
if '-k' in sys.argv:
    i = sys.argv.index('-k')
    k = int(sys.argv[i+1])
main(N, k)